* rpcclient -U Administrator%Ignite@123 192.168.1.172
* querydominfo - retrieves the domain, server, users on the system, and other relevant information.
* enumdomusers - extract the usernames with their RID.
* enumdomgroups - extract the groups with their RID.
* querygroup 0x200 - extract details about a particular group
* queryuser illwill - extracts information such as the username, Full name, Home Drive, Profile Path, Description, Logon Time, Logoff Time, Password set time, Password Change Frequency, RID, Groups, etc.
* enumprivs - enumerate user privileges
* getdompwinfo - enumerate the minimum password length and the enforcement of complex password rules.
* getusrdompwinfo 0x1f4 - command with the user’s RID.
* lsaenumsid - Enumerating SID from LSA
* createdomuser -  create a user within the domain
```
createdomuser hacker
setuserinfo2 illwill 24 Password@1
enumdomusers
```
* lookupnames illwill - check if the user we created has been assigned a SID or not
* enumalsgroups builtin - enumerates alias groups on the domain. The alias is an alternate name that can be used to reference an object or element. 
* deletedomuser illwill - Delete Domain User
* netshareenum
* netshareenumall
* netsharegetinfo Confidential - includes the path of the share, remarks, it will indicate if the share has a password for access, it will tell the number of users accessing the share and what kind of access is allowed on the share.
* enumdomains - enumerate all the domains
* enumdomgroups
* enumdomusers
* queryusersgroups 0x44f
* querygroupmem 0x201
* querydispinfo - obtain the users of the domain as well as the RID.
* chgpasswd - `chgpasswd raj Password@1 Password@987`
* createdomgroup newgroup
* deletedomgroup newgroup
* lookupdomain - `lookupdomain ignite` extract information about the domain,
* samlookupnames domain illwill
* samlookuprids domain 0x44f
* lsaenumsid - extract the details regarding the user that the SID belongs.
* lsaquery dsroledominfo
* LSA Query
```
lsaquery
dsroledominfo
```
* LSA Create Account
```
lookupnames raj
lsacreateaccount S\-1\-5\-21\-3232368669\-2512470540\-2741904768\-1103
```
* Enumerating LSA Group Privileges 
```
lsaenumsid
lookupsids S-1-1-0
lsaenumacctrights S-1-1-0
```
```
lsaaddpriv S-1-1-0 SeCreateTokenPrivilege
lsaenumprivsaccount S-1-1-0
lsadelpriv S-1-1-0 SeCreateTokenPrivilege
lsaenumprivsaccount S-1-1-0
```
* Enumerating LSA Account Privileges
```
lookupnames raj
lsaaddacctrights S-1-5-21-3232368669-2512470540-2741904768-1103 SeCreateTokenPrivilege
lsaenumprivsaccount S-1-5-21-3232368669-2512470540-2741904768-1103
lsaremoveacctrights S-1-5-21-3232368669-2512470540-2741904768-1103 SeCreateTokenPrivilege
lsaenumprivsaccount S-1-5-21-3232368669-2512470540-2741904768-1103
```
* lsaquerysecobj - enumerate the security objects or permissions and privileges related to the security

# References
https://www.hackingarticles.in/active-directory-enumeration-rpcclient/
